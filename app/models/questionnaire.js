import DS from 'ember-data';

export default DS.Model.extend({
  identifier: DS.attr(),
  name: DS.attr(),
  description: DS.attr(),
  categoryNameHyphenated: DS.attr(),
  questions: DS.hasMany('question')
});

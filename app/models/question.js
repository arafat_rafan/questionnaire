import DS from 'ember-data';

export default DS.Model.extend({
  questionType: DS.attr(),
  identifier: DS.attr(),
  headline: DS.attr(),
  description: DS.attr(),
  required: DS.attr(),
  multiple: DS.attr(),
  multiline: DS.attr(),
  choices: DS.attr(),
  jumps: DS.attr(),
});

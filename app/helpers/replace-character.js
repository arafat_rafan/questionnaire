import { helper } from '@ember/component/helper';

export function replaceCharacter(text) {
  return text[0].replace(/,| /g, "");
}

export default helper(replaceCharacter);

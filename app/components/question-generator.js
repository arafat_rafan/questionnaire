import Component from '@ember/component';
import Questionnaire from '../models/questionnaire';
import $ from 'jquery';

export default Component.extend({
  questionnaire: Questionnaire,
  isModelLoaded: false,
  title: '',
  init(){
    this._super(...arguments);
  },
  getElementValue(elementID){
    return this.$('#'+elementID).val();
  },
  getDestination(jumpsInfo, extraID){
    for(let i = 0; i < jumpsInfo.length; i++){
      let isConditionMatch = false;
      let conditions = jumpsInfo[i].conditions;
      for(let j = 0; j < conditions.length; j++){
        let data = conditions[j];
        let fieldValue = $('#'+data.field+extraID).val();
        if (fieldValue === data.value){
          isConditionMatch = true;
          break;
        }
      }
      if(isConditionMatch){
        return jumpsInfo[i].destination.id;
      }
    }
  },
  actions:{
    jumpToPosition(jumpsInfo, extraID){
      $("fieldset").removeClass("activeDiv");
        if(jumpsInfo && jumpsInfo.length > 0){
          let destinationID = this.getDestination(jumpsInfo, extraID);
          if(destinationID){
            $('html, body').animate({scrollTop:this.$('#'+destinationID+'div').offset().top}, 'slow');
            $('#'+destinationID+'div').parent('fieldset').addClass('activeDiv');
          }
        }
    }
  },

});

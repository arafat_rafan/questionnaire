import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Component | question-generator', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`{{question-generator}}`);

    assert.equal(this.element.textContent.trim(), '');

    // Template block usage:
    await render(hbs`
      {{#question-generator}}
        template block text
      {{/question-generator}}
    `);

    assert.equal(this.element.textContent.trim(), 'template block text');
  });
  test('should display title (Titleize) after setting title property', async function(assert) {
    assert.expect(2);

    await render(hbs`{{question-generator}}`);

    assert.equal(this.element.querySelector('label').textContent.trim(), '');

    this.set('titleText', 'clark assignments');
    await render(hbs`{{question-generator title=titleText}}`);
    assert.equal(this.element.querySelector('label').textContent.trim(), 'Clark Assignments');
  });
});
